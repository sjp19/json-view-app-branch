1. Create a virtual environment `python -m venv .venv`
2. Activate the virtual environment `source .venv/bin/activate`
3. Install requirements `pip install -r requirements.txt`
4. Run migrations `python manage.py migrate`
5. Load data `python manage.py loaddata demonstration/data.json`
6. Start your app server `python manage.py runserver`

1. Create a virtual environment `python -m venv .venv`
2. Activate the virtual environment `source .venv/bin/activate`
3. Install requirements `pip install -r requirements.txt`
4. Run migrations `python manage.py migrate`
5. Load data `python manage.py loaddata demonstration/data.json`
6. Start your app server `python manage.py runserver`


Branches:

# Main

This branch has the code prior to adding functionality for
additional request methods. Specifically,

Use that as a starting point to practice adding logic for
POST, CREATE, DELETE, and PUT methods.

#acl-layer-jsservice

This branch has code that includes the logic for the additional
methods, along with the code for incorporating data from jsservice.

